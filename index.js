import { Command } from 'commander';
import FeedParser from 'feedparser';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { markdown } from 'markdown';
import fetch from 'node-fetch';
import * as pathUtils from 'path';
import { Podcast } from 'podcast';
import youtubedl from 'youtube-dl-exec';

let youtubeDlInstance = () => {}; // init with dud

function simplifyMediaItem(item) {
    if (!item) {
        return null;
    }

    const mediaContentItems = [ 'title', 'description' ];
    const mediaContentUrls = [ 'content', 'thumbnail' ];
    const ytChannelMapping = {
        'yt:channelid': 'channelId',
    };
    const baseAttributes = [ 'date', 'guid', 'pubdate', 'pubDate', 'link', 'author' ];

    const mediaGroup = item['media:group'] || {};
    const simplifiedItem = {};

    mediaContentItems.forEach((name) => {
        const key = `media:${name}`;
        const valueContainer = mediaGroup[key];

        if (valueContainer) {
            simplifiedItem[name] = valueContainer['#'];
        } else {
            simplifiedItem[name] = null;
        }
    });

    mediaContentUrls.forEach((name) => {
        const key = `media:${name}`;
        const valueContainer = mediaGroup[key] && mediaGroup[key]['@'];

        if (valueContainer['url']) {
            simplifiedItem[name] = valueContainer['url'];
        } else {
            simplifiedItem[name] = null;
        }
    }); 

    Object.keys(ytChannelMapping).forEach(key => {
        const value = item[key] && item[key]['#'];
        const attr = ytChannelMapping[key];

        simplifiedItem[attr] = value || null;
    });

    baseAttributes.forEach(key => {
        simplifiedItem[key] = item[key] || null;
    });

    return simplifiedItem;
}

function readFeed(channelId) {
    return new Promise((resolve, reject) => {
        const url = `https://www.youtube.com/feeds/videos.xml?channel_id=${channelId}`;
        const req = fetch(url);
        const feedparser = new FeedParser({
            feedurl: url,
        });
        const items = [];
        const rawItems = [];
    
        feedparser.on('error', err => {
            reject(err);
        });
    
        feedparser.on('readable', function() {
            const stream = this;
            let item = null;
    
            while (item = stream.read()) {
                rawItems.push(item);
                items.push(simplifyMediaItem(item));
            }
        });

        feedparser.on('end', () => {
            fs.writeFileSync('example.json', JSON.stringify(items, null, 2));

            resolve(items);
        });
    
        req.then((res) => {
            if (res.status !== 200) {
                reject(res);
            }
    
            res.body.pipe(feedparser);
        });
    });
}

function sanitizeFileName(name) {
    const illegalRe = /[\/\?<>\\:\*\|"]/g;
    const controlRe = /[\x00-\x1f\x80-\x9f]/g;
    const reservedRe = /^\.+$/;
    const windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
    const windowsTrailingRe = /[\. ]+$/;

    return (name || '')
        .replace(/\'/g, '')
        .replace(/\`/g, '')
        .replace(illegalRe, '')
        .replace(controlRe, '')
        .replace(reservedRe, '')
        .replace(windowsReservedRe, '')
        .replace(windowsTrailingRe, '');
}

function downloadYoutubeVideo(item, config) {
    const url = item.link;
    const title = sanitizeFileName(item.title);
    const outputTemplate = `${config.youtubeDlOutputDirectory}${title}.%(ext)s` ;

    return youtubeDlInstance(url, {
        'extract-audio': true,
        'audio-format': config.youtubeDlOutputFileExtension,
        'embed-thumbnail': true,
        'audio-quality': 0,
        'output': outputTemplate,
    });
}

function generateFeedXml(config, items) {
    const feedUrl = `${config.baseUrl}/${config.feedFileName}`;
    const description = markdown.toHTML(config.description);
    const flatCategories = (config.categories || []).map(cat => cat.text);

    const feed = new Podcast({
        title: config.title,
        generator: 'yt2podcast',
        siteUrl: config.baseUrl,
        imageUrl: config.image,
        author: config.author,
        pubDate: new Date(),
        itunesAuthor: config.author,
        itunesSubtitle: config.subtitle,
        itunesSummary: description,
        itunesExplicit: config.explicitContent,
        itunesCategory: config.categories || [],
        categories: flatCategories,
        itunesImage: config.image,
        itunesType: config.type,
        feedUrl,
        description,
    });

    items.forEach(item => {
        const description = markdown.toHTML(item.description);
        const sanitizedTitle = sanitizeFileName(item.title);
        const podUrl = `${config.baseUrl}/${sanitizedTitle}.${config.youtubeDlOutputFileExtension}`;

        feed.addItem({
            title: item.title,
            description,
            url: item.link,
            guid: item.guid,
            author: config.author,
            date: item.pubDate || item.pubdate, // not sure if they can differ
            enclosure: {
                url: podUrl,
                file: item.localPath,
            },
            content: description,
            itunesAuthor: config.author,
            itunesExplicit: config.explicitContent,
            itunesSubtitle: config.subtitle,
            itunesSummary: description,
            itunesImage: item.thumbnail,

        });
    });

    return feed.buildXml();
}

const program = new Command();

program
    .version('0.0.1')
    .argument('<config>', 'absolute path to config file')
    .action(async (path) => {
        const config = yaml.load(fs.readFileSync(path));
        youtubeDlInstance = youtubedl.create(config.youtubeDlPath);

        console.log(`Starting to generate Podcast feed for ${config.title}`);

        const items = await readFeed(config.youtubeChannelId);

        console.log(`Found ${items.length} videos for channel https://www.youtube.com/channel/${config.youtubeChannelId}`);
        console.log('Starting video download. This may take a while...');

        let numOfDownloads = 0;
        const downloadedItems = [];

        for (const item of items) {
            numOfDownloads++;

            try {
                const title = sanitizeFileName(item.title);
                const outputName = `${title}.${config.youtubeDlOutputFileExtension}`;
                const outputPath = `${config.youtubeDlOutputDirectory}${outputName}`;

                if (fs.existsSync(outputPath)) {
                    console.log(`Video #${numOfDownloads} - ${item.title} already downloaded - omitting download`);
                } else {
                    console.log(`Starting download of video #${numOfDownloads} - ${item.title}`);
                    const output = await downloadYoutubeVideo(item, config);
                    console.log(`Finished downloading and convertig audio file`);
                }

                const downloadedItem = JSON.parse(JSON.stringify(item));
                downloadedItem.localPath = outputPath;
                downloadedItems.push(downloadedItem);
            } catch (e) {
                console.error(`Couldn't download video ${item.link}`, e);
                console.log('Omitting errorenous video, continuing...');
            }
        }

        const feedXml = generateFeedXml(config, downloadedItems);
        const feedOutputPath = pathUtils.join(
            config.youtubeDlOutputDirectory,
            config.feedFileName,
        );
        fs.writeFileSync(feedOutputPath, feedXml);

        console.log(`Written feed XML to ${config.feedFileName}`);
    });
program.parse(process.argv);
